import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, Button, ListView, AsyncStorage } from 'react-native';
import { Constants } from 'expo';

export default class App extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      date: '',
      volume: '',
      percentage: '',
      dataSource: ds.cloneWithRows([]),
      totalVolume: 0,
      totalPercentage: 0,
      totalAVolume: 0,
      weight: 80,
      weightInput: '',
      alcoholLevel: 0,
    };
    this._handleDeleteButtonPress = this._handleDeleteButtonPress.bind(this);
    this._retrieveData()
  }
  
  _storeData = async (data) => {
    try {
      await AsyncStorage.setItem('lastSave', data);
    } catch (error) {
      console.log(error);
    }
  };

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('lastSave');
      if (value !== null) {
        // We have data!!
        value = JSON.parse(value)
        const drinks = value.dataSource._dataBlob.s1;
        this.setState(() => ({
          date: value.date,
          volume: value.volume,
          percentage: value.percentage,
          dataSource: this.state.dataSource.cloneWithRows(drinks),
          totalVolume: value.totalVolume,
          totalPercentage: value.totalPercentage,
          totalAVolume: value.totalAVolume,
          weight: value.weight,
          weightInput: value.weightInput,
          alcoholLevel: value.alcoholLevel,
        }))
      }
    } catch (error) {
      console.log(error);
    }
  };

  _reloadData = () => {
    var totalVolume = 0
    var totalPercentage = 0
	  var totalAVolume = 0
    var date = new Date();

    this.state.dataSource._dataBlob.s1.map((data) => {
      totalVolume += parseFloat(data.volume)
      totalAVolume += parseFloat(data.percentage) * parseFloat(data.volume)
    });

    totalPercentage = totalAVolume / totalVolume
    
    if(this.state.dataSource._dataBlob.s1[0]){
      var firstDate = new Date(this.state.dataSource._dataBlob.s1[0].date)
    } else {
      var firstDate = date
    }
    
    var timeElapsed = date.getTime() - firstDate.getTime()
    var hoursElapsed = timeElapsed / 1000 / 60 / 60
    var aMass = (totalAVolume*1000) * 0.79
    var weight = this.state.weight
    aMass -= (weight/10)*hoursElapsed
    if(aMass<0) {
      aMass = 0
    }
    var alcoholLevel = aMass / weight


    this.setState({ 
      totalPercentage: totalPercentage,
      totalAVolume: totalAVolume,
      totalVolume: totalVolume,
      alcoholLevel: alcoholLevel
    });
  }

  _handleSendButtonPress = () => {
    if (!this.state.volume || !this.state.percentage) {
      return;
    }

    var volume = parseFloat(this.state.volume)
    var percentage = parseFloat(this.state.percentage) / 100
    
    var date = new Date();

    const textArray = this.state.dataSource._dataBlob.s1;

    textArray.push({
      date: date.toString(),
      volume: volume, 
      percentage: percentage
    });

    this.setState(() => ({
      dataSource: this.state.dataSource.cloneWithRows(textArray),
      volume: '',
      percentage: '',
    }));
    this._reloadData()
  };

  _handleWeightButtonPress = () => {
    if (this.state.weightInput) {
      this.setState({ 
        weight: this.state.weightInput,
        weightInput: '',
      });
    }
    this.state.weightInput = ""
    this._reloadData()
  }

  _handleDeleteButtonPress = (id) => {
    this.setState((a) => {
      const newItem = a.dataSource._dataBlob.s1.filter((item, i) => (parseInt(id) !== i));
      return {
        dataSource: this.state.dataSource.cloneWithRows(newItem),
      }
    });
  };

  render() {
    this._storeData(JSON.stringify(this.state))
    return (
      <View style={styles.container}>
        <View style={styles.formView}>
          <Text style={styles.header}>
            Hieno headeri
          </Text>
          <TextInput
            style={styles.inputForm}
            keyboardType='numeric'
            value={String(this.state.volume)}
            onChangeText={(volume)=> this.setState( {volume} )}
            placeholder="Volume"
          />
          <TextInput
            style={styles.inputForm}
            keyboardType='numeric'
            value={String(this.state.percentage)}
            onChangeText={(percentage)=> this.setState( {percentage} )}
            placeholder="Percentage"
          />
          <Button
            title="Add"
            onPress={this._handleSendButtonPress}
          />
            
          <Text style={styles.title}>Total vol: {this.state.totalVolume} l</Text>
          <Text style={styles.title}>Total per: {this.state.totalPercentage * 100}%</Text>
          <Text style={styles.title}>Total avol: {this.state.totalAVolume} l</Text>
          <Text style={styles.title}>Alcohol level: {this.state.alcoholLevel} ‰</Text>
          <Text style={styles.title}>Weight: {this.state.weight} kg</Text>
        </View>
        <View style={styles.formView}>
          <TextInput
            style={styles.inputForm}
            keyboardType='numeric'
            value={String(this.state.weightInput)}
            onChangeText={(weightInput)=> this.setState( {weightInput} )}
            placeholder="Weight"
          />
          <Button
            title="Update"
            onPress={this._handleWeightButtonPress}
          />
        </View>
        <ListView
          enableEmptySections={true}
          style={styles.listView}
          dataSource={this.state.dataSource}
          renderRow={(rowData, sectionID, rowID) => {
            const handleDelete = () => {
              this._handleDeleteButtonPress(rowID);
              this._reloadData(); // Why does this work before the line before?
            }
            return (
              <View style={styles.todoItem}>
                <Text style={styles.todoText}>Volume: {rowData.volume}</Text>
                <Text style={styles.todoText}>Percentage: {rowData.percentage * 100}%</Text>
                <Button
                  title="Delete"
                  onPress={handleDelete}
                  style={styles.deleteButton}
                />
              </View>
              );
            }
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#eee',
    color: '#000',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 5
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#eee',
  },
  formView: {
    borderBottomWidth: 1,
    borderColor: '#ccc',
    paddingBottom: 8,
  },
  inputForm: {
    backgroundColor: '#fff',
    width: 320,
    height: 40,
    padding: 8,
    marginBottom: 8,
  },
  todoItem: {
    alignItems: 'center',
    padding: 8,
    width: 320,
    borderBottomWidth: 1.5,
    borderColor: '#e0e0e0',
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'row',
  },
  todoText: {
    flex: 1,
  },
});
